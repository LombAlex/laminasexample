# Laminas Framework PHP example 2022

## About

I have created this project to learn how Laminas work.

## What I've done

- Create a new module called Album
- Connect the project to a database and display the content
- Create a form, then add/update/delte an element in the database
- Make a Test for the controller/model (with a mock) 
- Add a navigation menu, with the current location on the page
- Add a paginator (10 albums per page)
- Add few translation (just to know how it work)

## Tutos followed

https://docs.laminas.dev/tutorials/getting-started/skeleton-application/


## Installation on linux (based on Arch)

Install with pacman php 8 + libxcrypt-compat

https://www.apachefriends.org/download.html download run file here

```
Php:
7.4.28

Xamp:
port 80
folder /opt/lampp/htdocs/

Mysql port 3306
http://localhost/phpmyadmin/
```

Launch appache server (and Mysql):
```
sudo /opt/lampp/manager-linux-x64.run
```

Add Composer to Xampp (Composer is like Node in js)
```
curl -s https://getcomposer.org/installer | /opt/lampp/bin/php
```
(I've installed it inside /opt/lampp/htdocs/Laminas)

Call the php and composer from Xampp:
```
/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar
```

## Create Project/Module

Create project:
```
composer create-project -s dev laminas/laminas-mvc-skeleton directory_to_install

sudo /opt/lampp/bin/php ./composer.phar create-project -s dev laminas/laminas-mvc-skeleton /opt/lampp/htdocs/Laminas/laminasexample/
```

Create Album module (it need to looks like this):
```
project/
    /module
        /Album
            /config
            /src
                /Controller
                /Form
                /Model
            /view
                /album
                    /album
```
In /Album/src/Module.php, create the file like this:
```php
<?php
namespace Album;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
```

Add the module inside composer.json (autoload).

Run the command to load the package
```
composer dump-autoload

/opt/lampp/htdocs/Laminas/composer.phar dump-autoload
```

Inside the module, create the file /Album/config/module.config, then add the Controller and view.

## Others Commandes

```
composer development-enable

Use Docker: docker-compose up -d --build 
then 
docker-compose up -d

./vendor/bin/phpunit execute unitests

```

## Shortcuts

```
sudo /opt/lampp/bin/php -S 0.0.0.0:8080 -t public
```

http://0.0.0.0:8080/

Or 

```
sudo /opt/lampp/manager-linux-x64.run
```
http://localhost/Laminas/laminasexample/public/

Composer

```
/opt/lampp/bin/php /opt/lampp/htdocs/Laminas/composer.phar
```

Doc 

https://olegkrivtsov.github.io/laminas-api-reference/html/index.html

https://olegkrivtsov.github.io/laminas-api-reference/html/components/laminas/laminas-view.html

https://github.com/orgs/laminas/repositories

## Tips

Don't forget ```<?php``` !

```var_dump()``` or ```dd()``` to view a variable.

<br>

You can let the command ```sudo /opt/lampp/bin/php -S 0.0.0.0:8080 -t public``` running, you don't need to rerun it if you modify the code.

<br>

What's the differences between .phtml .php ?
There is usually no difference. When your web project grows bigger:
- PHP Page doesn't contain view-related code
- PHTML Page contains little (if any) data logic and the most part of it is presentation-related

<br>

Each page of the application is known as an action (like add, delete...) and actions are grouped into controllers within modules.

<br>

Model files:
One approach is to have model classes represent each entity in your application and then use mapper objects that load and save entities to the database. Another is to use an Object-Relational Mapping (ORM) technology, such as Doctrine or Propel.
In this project, I've used TableGateway.

<br>

I have remove the InvokableFactory of the module to access the property $table from within the controller whenever I need to interact with the model.

<br>

In view, always use the escapeHtml() view helper to help protect ourselves from Cross Site Scripting (XSS) vulnerabilities.

<br>

For the database, set it inside /config/autoload/global.php and /config/autoload/development.local.php.dist.
Actually, it use a Mysql database.

<br>
Execute my tests

```
/opt/lampp/bin/php ./vendor/bin/phpunit
or
/opt/lampp/bin/php ./vendor/bin/phpunit --testsuite Album
```

And add the folder in composer.json (autoload-dev) et dans phpunit.xml.dist
