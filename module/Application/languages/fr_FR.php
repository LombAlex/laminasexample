<?php

    return [
        'home'   => 'Accueil',
        'album' => 'Album',
        'myAlbums' => 'Mes albums',
        'english' => 'Anglais',
        'french' => 'Français',
    ];