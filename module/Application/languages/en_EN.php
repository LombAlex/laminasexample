<?php

    return [
        'home' => 'Home',
        'album' => 'Album',
        'myAlbums' => 'My albums',
        'english' => 'English',
        'french' => 'French',
    ];