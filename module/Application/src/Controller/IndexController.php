<?php

declare(strict_types=1);

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Locale;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $request = $this->getRequest();

        if ($request->getQuery('lang')) {
            $language = $request->getQuery('lang');
            Locale::setDefault($language);
        }

        return new ViewModel();
    }
}
