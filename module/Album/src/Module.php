<?php

namespace Album;

// For the Database
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
// For the module
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /*
    This method returns an array of factories that are all merged together by the ModuleManager
     before passing them to the ServiceManager.
    The factory for Album\Model\AlbumTable uses the ServiceManager to create an Album\Model\AlbumTableGateway
     service representing a TableGateway to pass to its constructor. We also tell the ServiceManager
     that the AlbumTableGateway service is created by fetching a Laminas\Db\Adapter\AdapterInterface
     implementation (also from the ServiceManager) and using it to create a TableGateway object. 
    The TableGateway is told to use an Album object whenever it creates a new result row.
    The TableGateway classes use the prototype pattern for creation of result sets and entities.
    This means that instead of instantiating when required, the system clones a previously instantiated object.
    */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\AlbumTable::class => function($container) {
                    $tableGateway = $container->get(Model\AlbumTableGateway::class);
                    return new Model\AlbumTable($tableGateway);
                },
                Model\AlbumTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Album());
                    return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    //
    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\AlbumController::class => function($container) {
                    return new Controller\AlbumController(
                        $container->get(Model\AlbumTable::class)
                    );
                },
            ],
        ];
    }
}